import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import LocationVO

def poll():
    while True:
        print("Poller is running")
        try:
            # Write your polling logic, here
            response = requests.get("http://wardrobe-api:8000/api/locations/") ## accessing this specific location
            content = json.loads(response.content) ## content is a dict
            print(content)
            for location in content["locations"]: ## iterating over content dictionary (the content is what you input in insomnia)
                LocationVO.objects.update_or_create(
                    import_href=location["href"],## reassign the import variable in the VO to this
                    defaults={"name": location["closet_name"]}, ## same thing
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
