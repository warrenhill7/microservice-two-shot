from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe

# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_URL",
        "id",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.name}

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "id"]

    def get_extra_data(self, o):
        return {"bin": o.bin.name}





@require_http_methods(["GET","POST"])
def api_list_shoes(request, shoe_id=None):

    if request.method == "GET":
        if shoe_id is not None:
            new_shoe = Shoe.objects.filter(id=shoe_id)
        else:
            new_shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoe": new_shoe},
            encoder=ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)
        print("********!!!!!ELSEBLOCK!!!!!", content)


        try:
            bin_href = content["bin"]
            print("*********TRYBOLCK2222222", bin_href)
            bin = BinVO.objects.get(import_href=f"/api/bins/{bin_href}/")
            print("********TRYBLOCK33333333", bin)
            content["bin"] = bin


        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin Id"},
                status=400
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    if request.method == "GET":
        new_shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            new_shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        try:
            new_shoe = Shoe.objects.get(id=id)
            new_shoe.delete()
            return JsonResponse(
                new_shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
