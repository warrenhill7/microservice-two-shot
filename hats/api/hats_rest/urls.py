from django.urls import path
from .views import api_list_hats, api_show_hat

urlpatterns = [
    path("hats/", api_list_hats, name="list_all_hats"), #showing list of all hats
    path("hats/", api_list_hats, name="api_create_hats"), #creating a hat
    path("hats/<int:pk>/", api_show_hat, name="api_show_hats"), #showing details and delete
]
