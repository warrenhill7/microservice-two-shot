import React, {useEffect, useState} from 'react';

function HatForm() {
    const [locations, setLocations] = useState([])

    const [name, setName] = useState("");
    const [fabric, setFabric] = useState("");
    const [color, setColor] = useState("");
    const [picture_url, setPictureurl] = useState("");
    const [location, setLocation] = useState("");

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureurl(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
    }

    useEffect(() => {
      fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = picture_url;
        data.location = location
        console.log(data)

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
          const newHat = await response.json();
          console.log(newHat)

          setName('');
          setFabric('');
          setColor('');
          setPictureurl('');
          setLocation('');
        }
      }


    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={picture_url} onChange={handlePictureUrlChange} placeholder="Url" required type="text" name="url" id="picture_url" className="form-control"/>
                  <label htmlFor="url">Url</label>
                </div>
                <div className="mb-3">
                  <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a wardrobe</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.closet_name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );

}

export default HatForm
