// import { Link } from 'react-router-dom'
// import { useState, useEffect} from 'react'

// const ShoeList = () => {
//     const [locations, setLocations] = useState([])

//     const getData = async () => {
//         const resp = await fetch('http://localhost:8000/api/locations/')
//         if (resp.ok) {
//             const data = await resp.json()
//             setLocations(data.locations)
//         }
//     }

//     useEffect(()=> {
//         getData()
//     }, [])

//     const handleDelete = async (e) => {
//         const url = `http://localhost:8000/api/locations/${e.target.id}`

//         const fetchConfigs = {
//             method: "Delete",
//             headers: {
//                 "Content-Type": "application/json"
//             }
//         }

//         const resp = await fetch(url, fetchConfigs)
//         const data = await resp.json()

//         setLocations(locations.filter(location => String(location.id) !== e.target.id))
//         // getData()
//     }

//     return <>
//         <div className="row">
//             <div className="offset-3 col-6">
//                 <div className="shadow p-4 mt-4">
//                     <h1>List Locations</h1>
//                     <table className="table table-striped">
//                         <thead>
//                             <tr>
//                                 <th>id</th>
//                                 <th>Location</th>
//                                 <th></th>
//                             </tr>
//                         </thead>

//                         <tbody>
//                             {
//                                 locations.map(location => {
//                                     return (
//                                     <tr key={location.href}>
//                                         <td>{ location.id }</td>
//                                         <td><Link to={`/locations/${location.id}`}>{ location.name }</Link></td>
//                                         <td><button onClick={handleDelete} id={location.id} className="btn btn-danger">Delete</button></td>
//                                     </tr>
//                                     );
//                                 })
//                             }
//                         </tbody>
//                     </table>
//                 </div>
//             </div>
//         </div>
//     </>
// }

// export default LocationList;
