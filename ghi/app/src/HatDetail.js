import { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';

const HatDetail = () => {
    const [detail, setDetail] = useState({})
    const { id } = useParams()

    const getData = async () => {
      const response = await fetch(`http://localhost:8090/api/hats/${id}`)
      if (response.ok) {
        const data = await response.json()
        setDetail(data)
      }
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
      <div>
        <table className="table table-striped">
          <thead>
            <tr>
                <th>Hat ID</th>
                <th>Name</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Wardrobe</th>
            </tr>
          </thead>
          <tbody>
                <tr>
                    <td>{ detail.id }</td>
                    <td>{ detail.name }</td>
                    <td>{ detail.fabric }</td>
                    <td>{ detail.color }</td>
                    <td>{ detail.location }</td>
                </tr>
          </tbody>
        </table>
        <div>
        <img src={ detail.picture_url } class="img-fluid img-thumbnail" alt=""></img>
        </div>
      </div>
    )
 }

export default HatDetail
