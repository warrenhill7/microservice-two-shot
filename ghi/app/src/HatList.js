import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function HatList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
      const response = await fetch('http://localhost:8090/api/hats/')
      if (response.ok) {
        const data = await response.json()
        setHats(data.hats)
      }
    }

    useEffect(()=>{
        getData()
    }, [])

    const handleDelete = async (e) => {
      const url = `http://localhost:8090/api/hats/${e.target.id}`

      const fetchConfigs = {
          method: "Delete",
          headers: {
              "Content-Type": "application/json"
          }
      }

      const resp = await fetch(url, fetchConfigs)
      const data = await resp.json()

      setHats(hats.filter(hat => String(hat.id) !== e.target.id))
      // getData()
    }

    return (
        <table className="table table-striped">
          <thead>
            <tr>
                <th>Hat id</th>
                <th>Name</th>
                <th>Waredrobe</th>
            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
              return(
                <tr key={hat.href}>
                    <td>{ hat.id }</td>
                    <td><Link to={`/hats/${hat.id}`}>{ hat.name }</Link></td>
                    <td>{ hat.location }</td>
                    <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Delete</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
 }

export default HatList
