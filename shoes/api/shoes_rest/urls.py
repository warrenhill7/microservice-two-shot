from django.urls import path

from .views import api_list_shoes,api_show_shoes

urlpatterns = [
    path("bins/", api_list_shoes, name="api_list_shoes"),

    path("bins/<int:id>/", api_show_shoes, name="show_shoe"),
]
