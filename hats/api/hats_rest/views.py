
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["name", "id",]

    def get_extra_data(self, o):
        return {"location": o.location.name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "name",
        "color",
        "picture_url",
        "id",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.name}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )

    else: #THIS IS THE POST REQUEST
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            print(location_href)
            location = LocationVO.objects.get(import_href=f"/api/locations/{location_href}/")
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )


        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except:
            return JsonResponse({"message": "Does not exist"})
