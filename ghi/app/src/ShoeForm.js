// import React, {useState, useEffect} from 'react';

// function ShoeForm() {
//   const [manufacturer, setManufacturer] = useState("")
//   const [name, setName] = useState("")
//   const [color, setColor] = useState("")
//   const [picture_URL, setPicture_URL] = useState("")
//   const [bin, setBin] = useState("")
//   const [bins, setBins] = useState([])

//     const handleSubmit = async (event) => {
//          event.preventDefault();

//         const data = {};

//         data.manufacturer = manufacturer;
//         data.name = name;
//         data.color = color;
//         data.picture_URL = picture_URL;
//         data.wardrobe = bin;

//         console.log(data);


//         const shoeURL = "http://localhost:8000/api/shoes/";
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//                 "Content-Type": "application/json",
//             },

//         };

//         const response = await fetch(shoeURL, fetchConfig);
//         if (response.ok) {
//             const newShoe = await response.json();
//             console.log(newShoe);
//             setManufacturer("");
//             setName("");
//             setColor("");
//             setPicture("");
//             setBin("");
//             }
//         };

//         const fetchData = async () => {
//             const url = "http://localhost:8100/api/bins/";

//             const response = await fetch(url);

//             if(response.ok) {
//                 const data = await response.json();
//                 setBins(data.bins);
//             }
//         };
//     }


//     const handleManufacturerChange = (event) => {
//         const value = event.target.value;
//         setManufacturer(value);
//     };

//     const handleNameChange = (event) => {
//         const value = event.target.value;
//     }



















//     return (
//         <div className="row">
//           <div className="offset-3 col-6">
//             <div className="shadow p-4 mt-4">
//               <h1>Create a new shoe</h1>
//               <form onSubmit={handleSubmit} id="create-conference-form">
//                 <div className="form-floating mb-3">
//                   <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
//                   <label htmlFor="name">manufacturer</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                   <input onChange={handleFormChange} value={formData.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
//                   <label htmlFor="starts">Name</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                   <input onChange={handleFormChange} value={formData.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
//                   <label htmlFor="ends">Color</label>
//                 </div>
//                 <div className="mb-3">
//                   <label htmlFor="description">Description</label>
//                   <textarea onChange={handleFormChange} value={formData.description} className="form-control" id="description" rows="3" name="description" className="form-control"></textarea>
//                 </div>
//                 <div className="form-floating mb-3">
//                   <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
//                   <label htmlFor="max_presentations">Maximum presentations</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                   <input onChange={handleFormChange}  value={formData.max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
//                   <label htmlFor="max_attendees">Maximum attendees</label>
//                 </div>
//                 <div className="mb-3">
//                   <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
//                     <option value="">Choose a location</option>
//                     {bins.map((bin) => {
//                       return (
//                         <option value={bin.href} value={bin.href}>{bin.name}</option>
//                       )
//                     })}
//                   </select>
//                 </div>
//                 <button className="btn btn-primary">Create</button>
//               </form>
//             </div>
//           </div>
//         </div>
//       );
//     }

//     export default ShoeForm;
